#include <QCoreApplication>
#include <QDebug>

#include <GPcapDeviceWrite>
#include <GDhcpHdr>

struct DhcpPacket {
	GEthHdr ethHdr_;
	GIpHdr ipHdr_;
	GUdpHdr udpHdr_;
	GDhcpHdr dhcpHdr_;
	char dummy[256];
};

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	GMac requestMac("e4:f8:9c:67:e4:cc");

	GPcapDeviceWrite write;
	write.open();

	DhcpPacket packet;

	GEthHdr* ethHdr = &packet.ethHdr_;
	ethHdr->dmac_ = GMac::broadcastMac();
	ethHdr->smac_ = write.intf()->mac();
	ethHdr->type_ = htons(GEthHdr::Ip4);

	GIpHdr* ipHdr = &packet.ipHdr_;
	ipHdr->v_hl_ = 0x45;
	ipHdr->tos_ = 0x10;
	// ipHdr->len_
	ipHdr->id_ = 0;
	ipHdr->off_ = htons(0x4000);
	ipHdr->ttl_ = 64;
	ipHdr->p_ = GIpHdr::Udp;
	// ipHdr->sum_
	ipHdr->sip_ = 0;
	ipHdr->dip_ = 0xFFFFFFFF;

	GUdpHdr* udpHdr = &packet.udpHdr_;
	udpHdr->sport_ = htons(68);
	udpHdr->dport_ = htons(67);
	// udpHdr->len_
	// udpHdr->sum_

	GDhcpHdr* dhcpHdr = &packet.dhcpHdr_;
	dhcpHdr->type_ = 1; // Boot Request
	dhcpHdr->hrd_ = 0x01; // Ethernet
	dhcpHdr->hln_= 6;
	dhcpHdr->hops_ = 0;
	dhcpHdr->transaction_ = htonl(0x11223344); // gilgil temp
	dhcpHdr->elapsed_ = 0;
	dhcpHdr->bootp_ = 0x0000;
	dhcpHdr->clientIp_ = 0x00000000;
	dhcpHdr->yourIp_ = 0x00000000;
	dhcpHdr->serverIp_ = 0x00000000;
	dhcpHdr->relayIp_ = 0x00000000;
	dhcpHdr->clientMac_ = requestMac;
	memset(dhcpHdr->padding_, 0, sizeof(dhcpHdr->padding_));
	memset(dhcpHdr->serverHostName_, 0, sizeof(dhcpHdr->serverHostName_));
	memset(dhcpHdr->bootFileName_, 0, sizeof(dhcpHdr->bootFileName_));
	dhcpHdr->magic_ = htonl(0x63825363);

	GDhcpHdr::Option* option = dhcpHdr->firstOption();
	option->type_ = GDhcpHdr::DhcpMessageType;
	option->len_ = 1;
	*pchar(option->value()) = 1; // DHCP: Discover(1)

	option = option->next();
	option->type_ = GDhcpHdr::ClientIdentifier;
	option->len_ = sizeof(GMac) + 1;
	*pbyte(option->value()) = 0x01; // Ethernet
	memcpy(pbyte(option->value()) + 1, &requestMac, sizeof(GMac));

	option = option->next();
	option->type_ = GDhcpHdr::End;

	uint32_t dhcpLen = pchar(option) - pchar(dhcpHdr) + 2;
	udpHdr->len_ = htons(sizeof(GUdpHdr) + dhcpLen);
	ipHdr->len_ = htons(sizeof(GIpHdr) + sizeof(GUdpHdr) + dhcpLen);

	udpHdr->sum_ = htons(GUdpHdr::calcChecksum(ipHdr, udpHdr));
	ipHdr->sum_ = htons(GIpHdr::calcChecksum(ipHdr));

	GBuf buf(pbyte(&packet), sizeof(GEthHdr) + sizeof(GIpHdr) + sizeof(GUdpHdr) + dhcpLen);
	write.write(buf);


	qDebug() << QString(packet.ethHdr_.smac()) << dhcpLen;
}
